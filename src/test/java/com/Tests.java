package com;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by Ira on 15.03.2016.
 */
public class Tests {
    @Test
    public void callGetColor() {
        Pen pen = new Pen(10);
        assertThat("Unsuccess call get color", pen.getColor(), is("BLUE"));
        assertThat("InkContainerValue is less or equal 0", pen.write("hello"), is("hello"));
    }

    @Test
    public void callWriteWhenIsWorkFalse() {
        Pen pen = new Pen(0);
        assertThat("InkContainerValue is more than 0", pen.write("hello"), is(""));
    }

    @Test
    public void callWriteWhanInkContainerValueLessNull() {
        Pen pen = new Pen(-5);
        assertThat("InkContainerValue is more than 0", pen.write("hello"), is(""));
    }

    @Test
    public void callWitInkContainerValueMoreThanSizeLetter() {
        Pen pen = new Pen(22, 2.3985240D);
        assertThat("InkContainerValue is less than sizeOfWord", pen.write("hello"), is("hello"));
        assertThat("InkContainerValue is less or equal 0", pen.isWork(), is(true));
    }

    @Test
    public void callInkContainerValueLessThanSizeLetter() {
        Pen pen = new Pen(2, 2.0D);
        assertThat("InkContainerValue is  less or more than expected result", pen.write("hello"), is("he"));
        assertThat("InkContainerValue is more than 0", pen.isWork(), is(false));
    }


    // ���!!!! �� ������ ������� � �� ���� �����, �� ����������� ������ ����
    @Test
    public void callInkContainerValueMuchLessThanSizeLetter() {
        Pen pen = new Pen(2, 2456678.85487650D);
        assertThat("InkContainerValue is  less or more than expected result", pen.write("hello"), is(""));
        assertThat("InkContainerValue is more than 0", pen.isWork(), is(false));
    }

    @Test
    public void callWitInkContainerValueEqualSizeLetterAndChangeColor() {
        Pen pen = new Pen(5, 1.0D, "RED");
        assertThat("InkContainerValue is  less or more than expected result", pen.write("hello"), is("hello"));
        assertThat("InkContainerValue is more than 0", pen.isWork(), is(false));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));
        pen.doSomethingElse();
        assertThat("Color is wrong",baos.toString().replace("\r\n",""), is("RED"));
    }
}
